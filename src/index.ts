
import * as express from "express";

const app = express.default();
let port = process.env.PORT || 3000;

// if port is a string which looks like a number convert to a number
// (Windows can be weird sometimes ...)
if (typeof port === "string" && /\d+/.test(port)) {
    port = Number(port);
}

app.get("/", (req, res) => res.send("hello, world!!"));

app.listen(port, () => console.log(`Listening on port ${port}!`));
